//
//  GameScene.swift
//  p06
//
//  Created by Ahmet on 4/8/17.
//  Copyright © 2017 Ahmet. All rights reserved.
//

import SpriteKit
import GameplayKit

var score = 0

class GameScene: SKScene, SKPhysicsContactDelegate {
    let accomplishments = [10, 20, 40, 70, 110, 160, 220, 290, 370, 460, 560]
    let score_label = SKLabelNode(fontNamed: "Cochin")
    let background = SKSpriteNode(imageNamed: "background.png")
    let bubble = SKSpriteNode(imageNamed: "bubble.png")
    let main_fish = SKSpriteNode(imageNamed: "fish.png")
    var turn_face_to = "left"
    
    let background_song = SKAudioNode(fileNamed: "loop.wav")
    let eat_sound = SKAction.playSoundFileNamed("eat.wav", waitForCompletion: false)
    
    let main_fish_category = 0x1 << 1
    let bait_fish_category = 0x1 << 2
    
    var looking_right = true
    
    var level = 0
    
    let xPlayerForce = 10
    let yPlayerForce = 10
    
    var main_fish_scale = 0.5

    var lastTouch: CGPoint? = nil
    
    var fish_velocity: CGFloat = 10.0
    var left_velocity: CGFloat = 1.0
    var up_velocity: CGFloat = 3.0
    
    var last_bubble_added: TimeInterval = 0
    var last_fish_added: TimeInterval = 0
    
    
    
    override func didMove(to view: SKView) {
        
        
        self.add_background()
        self.add_main_fish()
        self.add_score_label()
        
        self.physicsWorld.gravity = CGVector(dx:0, dy: 0)
        self.physicsWorld.contactDelegate = self
        self.addChild(background_song)
    }
    
    func add_background(){
        background.size = self.size
        background.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        background.zPosition = 0
        self.addChild(background)
    }
     
    func add_bubble(){
        let bubble = SKSpriteNode(imageNamed: "bubble")
        let random_size = CGFloat(Int(arc4random() % 50) + 50)
        bubble.size.width = random_size
        bubble.size.height = random_size
        bubble.name = "bubble"
        let random_x = Int(arc4random()) %  Int(self.size.width)
        bubble.position = CGPoint(x:  CGFloat(random_x), y: 20)
        bubble.zPosition = 1
        self.addChild(bubble)
    }
    func add_main_fish(){
        
        
        let atlas = SKTextureAtlas(named: "fish")
        let m1 = atlas.textureNamed("fish_01.png")
        let m2 = atlas.textureNamed("fish_02.png")
        
        let textures = [m1, m2]
        let meleeAnimation = SKAction.animate(with: textures, timePerFrame: 0.2)
        
        main_fish.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        main_fish.zPosition = 2
        main_fish.xScale = 0.50
        main_fish.yScale = 0.50
        main_fish.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: main_fish.size.width,
                                                                  height: main_fish.size.height))
        main_fish.physicsBody?.categoryBitMask = UInt32(main_fish_category)
        main_fish.physicsBody?.isDynamic = true
        main_fish.physicsBody?.contactTestBitMask = UInt32(bait_fish_category)
        main_fish.physicsBody?.collisionBitMask = 0
        main_fish.physicsBody?.affectedByGravity = false
        main_fish.run(SKAction.repeatForever(meleeAnimation))
        
        self.addChild(main_fish)
        
    }
    
    func add_score_label(){
        score_label.text = "Score: \(score)"
        score_label.fontSize = 75
        score_label.position = CGPoint(x: self.size.width * 0.1, y: self.size.height * 0.85)
        score_label.color = .white
        score_label.zPosition = 3
        self.addChild(score_label)
    }
    
    func add_bait(){
        let random = Int(arc4random() % 10)
        let new_bait = SKSpriteNode(imageNamed: "bait_0\(random).png")
        let max_size = UInt32(25 + level)
        
        let random_scale = CGFloat(Double(arc4random() % max_size + 25) / 100.0)
        new_bait.xScale = random_scale
        new_bait.yScale = random_scale
        new_bait.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: new_bait.size.width,
                                                                 height: new_bait.size.height))
        new_bait.physicsBody?.categoryBitMask = UInt32(bait_fish_category)
        new_bait.physicsBody?.isDynamic = true
        new_bait.physicsBody?.contactTestBitMask = UInt32(main_fish_category)
        new_bait.physicsBody?.collisionBitMask = 0
        new_bait.physicsBody?.affectedByGravity = false
        new_bait.physicsBody?.usesPreciseCollisionDetection = true
        
        
        let random_y = CGFloat(arc4random() % UInt32(self.size.height))
        var x_pos: CGFloat = 0
        
        if turn_face_to == "left"{
            x_pos = self.frame.size.width - 20
            new_bait.xScale = new_bait.xScale * -1
            new_bait.name = "left_bait"
            turn_face_to = "right" // next bait
        }else{
            x_pos = 20
            new_bait.name = "right_bait"
            turn_face_to = "left" // next bait

        }
        new_bait.position = CGPoint(x: x_pos, y: random_y)
        self.addChild(new_bait)
        level += 3
    }
    
    func move_bubble(){
        self.enumerateChildNodes(withName: "bubble", using: {(node, stop) -> Void in
            if let bubble = node as? SKSpriteNode{
                bubble.position = CGPoint(x:  bubble.position.x - self.left_velocity, y: bubble.position.y + self.up_velocity)
                if bubble.position.y <= 0{
                    bubble.removeFromParent()
                }
            }
        })
    }
    func move_baits(){
        self.enumerateChildNodes(withName: "left_bait", using: {(node, stop) -> Void in
            if let left_bait = node as? SKSpriteNode{
                left_bait.position = CGPoint(x:  left_bait.position.x - self.fish_velocity, y: left_bait.position.y)
                if left_bait.position.x <= 0{
                    left_bait.removeFromParent()
                }
            }
        })
        self.enumerateChildNodes(withName: "right_bait", using: {(node, stop) -> Void in
            if let right_bait = node as? SKSpriteNode{
                right_bait.position = CGPoint(x:  right_bait.position.x + self.fish_velocity, y: right_bait.position.y)
                if right_bait.position.x >= self.frame.width{
                    right_bait.removeFromParent()
                }
            }
        })
    }

    func game_over_reached(){
        let game_over_scene = GameOverScene(size: self.size)
        game_over_scene.scaleMode = self.scaleMode
        
        let scene_transition = SKTransition.crossFade(withDuration: 0.2)
        self.view!.presentScene(game_over_scene, transition: scene_transition)
        
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        var first_body = SKPhysicsBody()
        var second_body = SKPhysicsBody()
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask{
            first_body = contact.bodyA
            second_body = contact.bodyB
        }else{
            first_body = contact.bodyB
            second_body = contact.bodyA
        }
        
        if (first_body.categoryBitMask & UInt32(main_fish_category) != 0 &&
            second_body.categoryBitMask & UInt32(bait_fish_category) != 0){
                // if main fish is larger than bait
            if (first_body.node?.frame.size.height)! >= (second_body.node?.frame.size.height)! {
            //if ((first_body.node?.frame.size.height)! * (first_body.node?.frame.size.width)!) >= ((second_body.node?.frame.size.height)!  * (second_body.node?.frame.size.width)!){
                main_fish_scale += 0.02
                let getbigger = SKAction.scale(to: CGFloat(main_fish_scale), duration: 0.5)
                first_body.node?.run(getbigger)
//                if !looking_right{
//                    looking_right = true
//                    main_fish.xScale = main_fish.xScale * -1
//                }else{
//                    looking_right = false
//                    main_fish.xScale = main_fish.xScale * -1
//                }

                second_body.node?.removeFromParent()
                score += 1
                score_label.text = "Score: \(score)"
                self.run(eat_sound)
            }else{
                self.game_over_reached()
            }
            

        }
        
    }
    func check_accomplishment(){
        if score >= accomplishments[selected_level]{
            let congratulations_scene = Congratulations(size: self.size)
            congratulations_scene.scaleMode = self.scaleMode
            
            let scene_transition = SKTransition.crossFade(withDuration: 0.2)
            self.view!.presentScene(congratulations_scene, transition: scene_transition)

        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            lastTouch = touch.location(in: self)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            lastTouch = touch.location(in: self)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        lastTouch = nil
    }
    

    override func update(_ currentTime: TimeInterval) {
        self.check_accomplishment()
        self.move_bubble()
        self.move_baits()
        if currentTime - self.last_bubble_added > 1.0 {
            self.last_bubble_added = currentTime + 1.0
            self.add_bubble()
        }
        
        if (currentTime - self.last_fish_added) > (2.0 / Double((selected_level+1) * 3)) {
            self.last_fish_added = currentTime + 1.0
            self.add_bait()
        }

        
        if main_fish.position.x < 20 {
            main_fish.position.x = 30
        }
        else if main_fish.position.x > self.size.width - 20{
            main_fish.position.x = self.size.width - 30
        }
        if main_fish.position.y < 20 {
            main_fish.position.y = 30
        }
        else if main_fish.position.y > self.size.height - 20{
            main_fish.position.y = self.size.height - 30
        }

        
        if let touch = lastTouch {
            var xForce = 0.0
            var yForce = 0.0
            let xTouchOffset = (touch.x - main_fish.position.x)
            let yTouchOffset = (touch.y - main_fish.position.y)
            
            if xTouchOffset > 0.0 {
                xForce = Double(xPlayerForce)
                if !looking_right{
                    looking_right = true
                    main_fish.xScale = main_fish.xScale * -1
                }
            }
            else if xTouchOffset < 0.0 {
                xForce = -(Double)(xPlayerForce)
                if looking_right{
                    looking_right = false
                    main_fish.xScale = main_fish.xScale * -1
                }

            }
            
            if yTouchOffset > 0.0 {
                yForce = Double(yPlayerForce)
            }
            else if yTouchOffset < 0.0{
                yForce = -(Double)(yPlayerForce)
            }
            
            
            let impulseVector = CGVector(dx: xForce, dy: yForce)
            main_fish.physicsBody?.applyImpulse(impulseVector)
        }
    }
}
