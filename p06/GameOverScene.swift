//
//  GameOverScene.swift
//  p05
//
//  Created by Ahmet on 3/22/17.
//  Copyright © 2017 Ahmet. All rights reserved.
//

import Foundation
import SpriteKit

class GameOverScene: SKScene{
    let game_over_label = SKLabelNode(fontNamed: "Cochin")
    let score_label = SKLabelNode(fontNamed: "Cochin")
    let high_score_label = SKLabelNode(fontNamed: "Cochin")
    let restart_label = SKLabelNode(fontNamed: "Cochin")
    
    
    let tap_sound = SKAction.playSoundFileNamed("tap.wav", waitForCompletion: false)
    
    override func didMove(to view: SKView) {
        game_over_label.text = "Game Over"
        game_over_label.fontSize = 250
        game_over_label.position = CGPoint(x: self.size.width / 2, y: self.size.height * 0.8)
        game_over_label.color = .white
        self.addChild(game_over_label)
        
        score_label.text = "Score: \(score)"
        score_label.fontSize = 150
        score_label.position = CGPoint(x: self.size.width / 2, y: self.size.height * 0.6)
        score_label.color = .white
        self.addChild(score_label)
        
        let saved = UserDefaults()
        var high_score = saved.integer(forKey: "high_score")
        
        if score > high_score{
            high_score = score
            saved.set(high_score, forKey: "high_score")
        }
        score = 0
        
        high_score_label.text = "High Score: \(high_score)"
        high_score_label.fontSize = 100
        high_score_label.position = CGPoint(x: self.size.width / 2, y: self.size.height * 0.5)
        high_score_label.color = .white
        self.addChild(high_score_label)
        
        restart_label.text = "Restart"
        restart_label.fontSize = 150
        restart_label.position = CGPoint(x: self.size.width / 2, y: self.size.height * 0.35)
        restart_label.color = .white
        restart_label.name = "restart"
        self.addChild(restart_label)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches{
            let node = atPoint(touch.location(in: self))
            let name = node.name
            
            if name == "restart"{
                self.run(tap_sound)
                let game_scene = GameScene(size: self.size)
                game_scene.scaleMode = self.scaleMode
                
                let scene_transition = SKTransition.crossFade(withDuration: 0.2)
                self.view!.presentScene(game_scene, transition: scene_transition)
            }
        }
    }
}
