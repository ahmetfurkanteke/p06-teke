//
//  Congratulations.swift
//  p06
//
//  Created by Ahmet on 4/14/17.
//  Copyright © 2017 Ahmet. All rights reserved.
//

import Foundation
import SpriteKit

class Congratulations: SKScene{
    let tap_sound = SKAction.playSoundFileNamed("tap.wav", waitForCompletion: false)
    let hello_label = SKLabelNode(fontNamed: "Cochin")
    let explanation_label = SKLabelNode(fontNamed: "Cochin")
    let play_label = SKLabelNode(fontNamed: "Cochin")
    let high_score_label = SKLabelNode(fontNamed: "Cochin")
    
    override func didMove(to view: SKView) {
        let saved = UserDefaults()
        let possible_level = Int(saved.integer(forKey: "possible_level"))
        
        hello_label.text = "Congratulations!"
        hello_label.fontSize = 250
        hello_label.position = CGPoint(x: self.size.width / 2, y: self.size.height * 0.8)
        hello_label.color = .white
        hello_label.name  = "non_clickable"
        self.addChild(hello_label)
        
        explanation_label.text = "You won this level: \(selected_level+1)"
        explanation_label.fontSize = 100
        explanation_label.position = CGPoint(x: self.size.width / 2, y: self.size.height * 0.6)
        explanation_label.color = .white
        explanation_label.name  = "non_clickable"
        self.addChild(explanation_label)
        
        var high_score = saved.integer(forKey: "high_score")

        if score > high_score{
            high_score = score
            saved.set(high_score, forKey: "high_score")
        }
        NSLog("Selected: \(selected_level)/")
        selected_level += 1
        if selected_level > possible_level {
            saved.set(selected_level, forKey: "possible_level")
        }
        
        high_score_label.text = "High Score: \(high_score)"
        high_score_label.fontSize = 100
        high_score_label.position = CGPoint(x: self.size.width / 2, y: self.size.height * 0.5)
        high_score_label.color = .white
        high_score_label.name  = "non_clickable"
        self.addChild(high_score_label)
        play_label.text = "Select a Level"
        play_label.fontSize = 150
        play_label.position = CGPoint(x: self.size.width / 2, y: self.size.height * 0.35)
        play_label.color = .white
        play_label.name  = "non_clickable"
        self.addChild(play_label)
        
        for i in 1...10{
            add_level(level: i)
        }
        
        
        score = 0
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches{
            let node = atPoint(touch.location(in: self))
            let name = node.name
            
            if name != "non_clickable"{
                let game_scene = GameScene(size: self.size)
                game_scene.scaleMode = self.scaleMode
                
                let scene_transition = SKTransition.crossFade(withDuration: 0.2)
                self.view!.presentScene(game_scene, transition: scene_transition)
                selected_level = Int(name!)!
                self.run(tap_sound)
            }
        }
    }
    
    func add_level(level: Int){
        let saved = UserDefaults()
        let possible_level = Int(saved.integer(forKey: "possible_level"))
        let shape = SKShapeNode()
        shape.path = UIBezierPath(roundedRect: CGRect(x: -128, y: -128, width: 128, height: 128), cornerRadius: 64).cgPath
        var x_pos: CGFloat
        var y_pos: CGFloat
        if level > 5{
            y_pos = self.size.height * 0.15
            x_pos = 500 + CGFloat((level-5) * 200)
        }else{
            y_pos = self.size.height * 0.30
            x_pos = 500 + CGFloat(level * 200)
            
        }
        shape.position = CGPoint(x: x_pos, y: y_pos)
        if possible_level >= level-1{
            shape.fillColor = UIColor.yellow
            shape.name = "\(level)"
        }else{
            shape.fillColor = UIColor.gray
            shape.name = "non_clickable"
        }
        shape.strokeColor = UIColor.gray
        shape.lineWidth = 10
        addChild(shape)
        
        let level_label = SKLabelNode(fontNamed: "Cochin")
        level_label.text = "\(level)"
        level_label.fontSize = 100
        level_label.position = CGPoint(x: x_pos-65, y: y_pos-100)
        if possible_level >= level-1{
            level_label.fontColor = UIColor.gray
            level_label.name = "\(level)"
        }else{
            level_label.fontColor = UIColor.black
            level_label.name = "non_clickable"
        }
        addChild(level_label)
    }
}
